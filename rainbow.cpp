#include "rainbow.h"

#include "Timer.h"

Rainbow::Rainbow(float changePerSecond) {
  m_changePerSecond = changePerSecond;
}

Pixel Rainbow::update() {
  float currentTime = Timer::getCurrentTime();
  m_currentHue += m_changePerSecond * (currentTime - m_lastTime_s);
  m_lastTime_s = currentTime;

  if (m_currentHue > 360.0) {
    m_currentHue = 0.0;
  }

  return Pixel::hsvPixel(m_currentHue, 1.0, 1.0);
}