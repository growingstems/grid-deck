#include <stdint.h>
#include "pixel.h"
#include "grid_selection.h"
#include "grid_space.h"
#include "Adafruit_NeoTrellis.h"
#include "controls.h"
#include "rainbow.h"
#include "pulse_width.h"

#pragma once

class Buttons {
  public:
    void begin();

    void setAllPixels(uint32_t color);
    void setPixel(Pixel pixel, int x, int y);
    void setPixel(GridSelection selection, GridPosition position, int x, int y, bool blinkBlank);
    void read();
    void update();
    void feedHeartbeatWatchdog(bool feed);
    uint8_t* getBytes();
  private:
    static const int k_gridOffsetX = 2;
    static const int k_gridOffsetY = 1;
    void clear();
    void updatePixels();

    GridSpace m_grid = GridSpace();
    Controls::IntakePositionsEnum m_intakePosition = Controls::INTAKE_POS_NONE;
    Controls::IntakeAdjustEnum m_intakeAdjust = Controls::INTAKE_ADJUST_NONE;
    Controls::BumpsEnum m_armBump = Controls::BUMPS_NONE;
    bool m_disableSensors = false;
    bool m_stackMode = false;
    bool m_stackMode2 = false;
    bool m_fixCone = false;

    static const int k_normalRainbow = 80.0;
    Rainbow m_rainbow = Rainbow(k_normalRainbow);
    Rainbow m_fastRainbow = Rainbow(k_normalRainbow * 6.0);

    PulseWidth m_flash = PulseWidth(0.5, 0.5);

    float m_watchdogLastFed = 0.0;
    bool m_watchdogFed = false;
    static const int k_watchdogExceededTime = 2.0;
    PulseWidth m_watchdogNotFedFlash = PulseWidth(1.5, 0.15);
};