#include "grid_position.h"

GridPosition::GridPosition(const NodePositionEnum &pos) {
  mPosition = pos;
  mValid = true;

  switch (pos) {
    case LEFT_UPPER_LEFT:
      mX = 0;
      mY = 0;
      break;
    case LEFT_UPPER_CENTER:
      mX = 1;
      mY = 0;
      break;
    case LEFT_UPPER_RIGHT:
      mX = 2;
      mY = 0;
      break;
    case LEFT_CENTER_LEFT:
      mX = 0;
      mY = 1;
      break;
    case LEFT_CENTER:
      mX = 1;
      mY = 1;
      break;
    case LEFT_CENTER_RIGHT:
      mX = 2;
      mY = 1;
      break;
    case LEFT_BOTTOM_LEFT:
      mX = 0;
      mY = 2;
      break;
    case LEFT_BOTTOM_CENTER:
      mX = 1;
      mY = 2;
      break;
    case LEFT_BOTTOM_RIGHT:
      mX = 2;
      mY = 2;
      break;
    case COOP_UPPER_LEFT:
      mX = 3;
      mY = 0;
      break;
    case COOP_UPPER_CENTER:
      mX = 4;
      mY = 0;
      break;
    case COOP_UPPER_RIGHT:
      mX = 5;
      mY = 0;
      break;
    case COOP_CENTER_LEFT:
      mX = 3;
      mY = 1;
      break;
    case COOP_CENTER:
      mX = 4;
      mY = 1;
      break;
    case COOP_CENTER_RIGHT:
      mX = 5;
      mY = 1;
      break;
    case COOP_BOTTOM_LEFT:
      mX = 3;
      mY = 2;
      break;
    case COOP_BOTTOM_CENTER:
      mX = 4;
      mY = 2;
      break;
    case COOP_BOTTOM_RIGHT:
      mX = 5;
      mY = 2;
      break;
    case RIGHT_UPPER_LEFT:
      mX = 6;
      mY = 0;
      break;
    case RIGHT_UPPER_CENTER:
      mX = 7;
      mY = 0;
      break;
    case RIGHT_UPPER_RIGHT:
      mX = 8;
      mY = 0;
      break;
    case RIGHT_CENTER_LEFT:
      mX = 6;
      mY = 1;
      break;
    case RIGHT_CENTER:
      mX = 7;
      mY = 1;
      break;
    case RIGHT_CENTER_RIGHT:
      mX = 8;
      mY = 1;
      break;
    case RIGHT_BOTTOM_LEFT:
      mX = 6;
      mY = 2;
      break;
    case RIGHT_BOTTOM_CENTER:
      mX = 7;
      mY = 2;
      break;
    case RIGHT_BOTTOM_RIGHT:
      mX = 8;
      mY = 2;
      break;
  }
}

GridPosition::GridPosition(const int &x, const int &y) {
  if (x < kMinX || x > kMaxX || y < kMinY || y > kMaxY) {
    mValid = false;
    return;
  }

  mValid = true;

  if (x == 0 && y == 0) {
    mPosition = RIGHT_BOTTOM_RIGHT;
  } else if (x == 1 && y == 0) {
    mPosition = RIGHT_BOTTOM_CENTER;
  } else if (x == 2 && y == 0) {
    mPosition = RIGHT_BOTTOM_LEFT;
  } else if (x == 3 && y == 0) {
    mPosition = COOP_BOTTOM_RIGHT;
  } else if (x == 4 && y == 0) {
    mPosition = COOP_BOTTOM_CENTER;
  } else if (x == 5 && y == 0) {
    mPosition = COOP_BOTTOM_LEFT;
  } else if (x == 6 && y == 0) {
    mPosition = LEFT_BOTTOM_RIGHT;
  } else if (x == 7 && y == 0) {
    mPosition = LEFT_BOTTOM_CENTER;
  } else if (x == 8 && y == 0) {
    mPosition = LEFT_BOTTOM_LEFT;
  } else if (x == 0 && y == 1) {
    mPosition = RIGHT_CENTER_RIGHT;
  } else if (x == 1 && y == 1) {
    mPosition = RIGHT_CENTER;
  } else if (x == 2 && y == 1) {
    mPosition = RIGHT_CENTER_LEFT;
  } else if (x == 3 && y == 1) {
    mPosition = COOP_CENTER_RIGHT;
  } else if (x == 4 && y == 1) {
    mPosition = COOP_CENTER;
  } else if (x == 5 && y == 1) {
    mPosition = COOP_CENTER_LEFT;
  } else if (x == 6 && y == 1) {
    mPosition = LEFT_CENTER_RIGHT;
  } else if (x == 7 && y == 1) {
    mPosition = LEFT_CENTER;
  } else if (x == 8 && y == 1) {
    mPosition = LEFT_CENTER_LEFT;
  } else if (x == 0 && y == 2) {
    mPosition = RIGHT_UPPER_RIGHT;
  } else if (x == 1 && y == 2) {
    mPosition = RIGHT_UPPER_CENTER;
  } else if (x == 2 && y == 2) {
    mPosition = RIGHT_UPPER_LEFT;
  } else if (x == 3 && y == 2) {
    mPosition = COOP_UPPER_RIGHT;
  } else if (x == 4 && y == 2) {
    mPosition = COOP_UPPER_CENTER;
  } else if (x == 5 && y == 2) {
    mPosition = COOP_UPPER_LEFT;
  } else if (x == 6 && y == 2) {
    mPosition = LEFT_UPPER_RIGHT;
  } else if (x == 7 && y == 2) {
    mPosition = LEFT_UPPER_CENTER;
  } else if (x == 8 && y == 2) {
    mPosition = LEFT_UPPER_LEFT;
  } else {
    mValid = false;
  }
}

int GridPosition::getX() const {
  return mX;
}

int GridPosition::getY() const {
  return mY;
}

GridPosition::NodePositionEnum GridPosition::getEnum() const {
  return mPosition;
}

bool GridPosition::isValid() const {
  return mValid;
}

GridPosition::NodePositionTypeEnum GridPosition::getPositionType() {
  switch (mPosition) {
    case LEFT_UPPER_LEFT:
      return CONE;
    case LEFT_UPPER_CENTER:
      return CUBE;
    case LEFT_UPPER_RIGHT:
      return CONE;
    case LEFT_CENTER_LEFT:
      return CONE;
    case LEFT_CENTER:
      return CUBE;
    case LEFT_CENTER_RIGHT:
      return CONE;
    case LEFT_BOTTOM_LEFT:
      return HYBRID;
    case LEFT_BOTTOM_CENTER:
      return HYBRID;
    case LEFT_BOTTOM_RIGHT:
      return HYBRID;
    case COOP_UPPER_LEFT:
      return CONE;
    case COOP_UPPER_CENTER:
      return CUBE;
    case COOP_UPPER_RIGHT:
      return CONE;
    case COOP_CENTER_LEFT:
      return CONE;
    case COOP_CENTER:
      return CUBE;
    case COOP_CENTER_RIGHT:
      return CONE;
    case COOP_BOTTOM_LEFT:
      return HYBRID;
    case COOP_BOTTOM_CENTER:
      return HYBRID;
    case COOP_BOTTOM_RIGHT:
      return HYBRID;
    case RIGHT_UPPER_LEFT:
      return CONE;
    case RIGHT_UPPER_CENTER:
      return CUBE;
    case RIGHT_UPPER_RIGHT:
      return CONE;
    case RIGHT_CENTER_LEFT:
      return CONE;
    case RIGHT_CENTER:
      return CUBE;
    case RIGHT_CENTER_RIGHT:
      return CONE;
    case RIGHT_BOTTOM_LEFT:
      return HYBRID;
    case RIGHT_BOTTOM_CENTER:
      return HYBRID;
    case RIGHT_BOTTOM_RIGHT:
    default:
      return HYBRID;
  }
}

Pixel GridPosition::getDefaultPixel() {
  switch (getPositionType()) {
    case CONE:
      return Pixel::k_yellow;
    case CUBE:
      return Pixel::k_purple;
    case HYBRID:
      return Pixel::k_orange;
    default:
      return Pixel::k_off;
  }
}