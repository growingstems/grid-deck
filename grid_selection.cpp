#include "grid_selection.h"

GridSelection::GridSelection(const GridSelectionEnum &selection) {
  m_selection = selection;
}

GridSelection::GridSelectionEnum GridSelection::getEnum() const {
  return m_selection;
}

Pixel GridSelection::getDefaultPixel(GridPosition position, bool blinkBlank) const {
  Pixel pixel;
  GridSelectionEnum selection = m_selection;

  if (blinkBlank) {
     selection = NONE;
  }

  switch (selection) {
      case NONE:
      default:
        pixel = position.getDefaultPixel();
        pixel.setBrightness(0.02);
        return pixel;
      case SELECTED:
        return Pixel::k_selected;
      case SCORED:
        pixel = Pixel::k_green;
        pixel.setBrightness(0.25);
        return pixel;
      case RECOMMENDED:
        pixel = Pixel::k_magenta;
        pixel.setBrightness(0.25);
        return pixel;
      case PLANNED_FOR_TEAMMATE:
        pixel = Pixel::k_cyan;
        pixel.setBrightness(0.02);
        return pixel;
  }
}