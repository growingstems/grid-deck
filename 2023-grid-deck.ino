#include "buttons.h"
#include "grid_selection.h"
#include "grid_position.h"
#include "robot_packet.h"
#include "timer.h"

#define BRIGHTNESS 0.03

#define TRELLIS_INT_PIN 32
#define BATTERY_VOLT_PIN 35

Buttons buttons;
bool blinkBlank = false;
Timer sendTimer = Timer();
const float sendTime = 0.02;

void setup() {
  Serial.begin(921600);
  buttons.begin();
}

void loop() {
  if (Serial.available() > 0) {
    while (Serial.read() >= 0) {
    }
    buttons.feedHeartbeatWatchdog(true);
  } else {
    buttons.feedHeartbeatWatchdog(false);    
  }
  
  buttons.update();

  if (sendTimer.hasPeriodPassed(sendTime)) {
    uint8_t* bites = buttons.getBytes();

    Serial.write(bites[0]);
    Serial.write(bites[1]);
    Serial.write(bites[2]);
    Serial.write(bites[3]);
    Serial.write(bites[4]);
    Serial.write(bites[5]);
    Serial.write(bites[6]);
    Serial.write('\n');
  }
}
