#pragma once

class Controls {
  public:
    enum IntakePositionsEnum {
      INTAKE_POS_NONE = 0b000,
      GROUND_YELLOW = 0b001,
      GROUND_PURPLE = 0b010,
      TIPPED_YELLOW = 0b011,
      SHELF_YELLOW = 0b100,
      CHUTE_YELLOW = 0b101,
      CHUTE_PURPLE = 0b110,
      LIAM_TIPPED_YELLOW = 0b111
    };
    
    enum IntakeAdjustEnum {
      INTAKE_ADJUST_NONE = 0b00,
      INTAKE_CONE_OUT = 0b01,
      INTAKE_CUBE_OUT = 0b10
    };

    enum BumpsEnum {
      BUMPS_NONE = 0b000,
      OUT = 0b001,
      IN = 0b010,
      LEFT = 0b011,
      RIGHT = 0b100
    };
};