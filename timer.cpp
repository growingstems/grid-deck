#include "timer.h"

#include <Arduino.h>

Timer::Timer() {
  start();
}

void Timer::start() {
  if (!m_running) {
    reset();
    m_running = true;
  }
}

void Timer::stop() {
  if (m_running) {
    m_running = false;
  }
}

void Timer::reset() {
  m_startTime = getCurrentTime();
}

float Timer::get() {
  if (m_running) {
    return getCurrentTime() - m_startTime;
  } else {
    return -1.0;
  }
}

bool Timer::hasElapsed(float time) {
  if (!m_running) {
    return false;
  } else {
    return get() > time;
  }
}

bool Timer::hasPeriodPassed(float time) {
  bool elapsed = false;

  while (hasElapsed(time)) {
    m_startTime += time;
    elapsed = true;
  }
  
  return elapsed;
}
    
float Timer::getCurrentTime() {
  return ((float)millis()) / 1.0e3;
}