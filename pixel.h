#pragma once

const float k_defaultBrightness = 0.13;

class Pixel {
  public:
    Pixel(float r=0.0, float g=0.0, float b=0.0, float brightness=k_defaultBrightness);

    static const Pixel k_off;
    static const Pixel k_red;
    static const Pixel k_green;
    static const Pixel k_blue;
    static const Pixel k_cyan;
    static const Pixel k_magenta;
    static const Pixel k_yellow;
    static const Pixel k_purple;
    static const Pixel k_orange;
    static const Pixel k_selected;

    static Pixel hsvPixel(float h, float s, float v);
    float getLuminance();

    void setBrightness(float brightness);

    float getRed() const;
    float getGreen() const;
    float getBlue() const;

    bool operator == (const Pixel &p);
    
  private:
    float m_r;
    float m_g;
    float m_b;
    float m_brightness;

    void setRed(float r);
    void setGreen(float g);
    void setBlue(float b);
};