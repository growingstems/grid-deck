#include "pixel.h"
#include "grid_position.h"

#pragma once

class GridSelection {
  public:
    enum GridSelectionEnum {
      NONE,
      SELECTED,
      SCORED,
      RECOMMENDED,
      PLANNED_FOR_TEAMMATE
    };

    GridSelection(const GridSelectionEnum &selection);

    GridSelectionEnum getEnum() const;
    
    Pixel getDefaultPixel(GridPosition position, bool blinkBlank) const;

  private:
    GridSelectionEnum m_selection;
};