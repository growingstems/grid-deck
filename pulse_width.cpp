#include "pulse_width.h"

PulseWidth::PulseWidth(float period, float dutyCycle) {
  m_period = period;
  m_dutyCycle = dutyCycle;
}

bool PulseWidth::getPulseState() {
  m_timer.hasPeriodPassed(m_period);
  return m_timer.get() < (m_period * m_dutyCycle);
}