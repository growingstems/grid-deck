#include "pixel.h"

#include <cmath>

#define EPS 1.0e-6

const Pixel Pixel::k_off{0.0, 0.0, 0.0};
const Pixel Pixel::k_red{1.0, 0.0, 0.0};
const Pixel Pixel::k_green{0.0, 1.0, 0.0};
const Pixel Pixel::k_blue{0.0, 0.0, 1.0};
const Pixel Pixel::k_cyan{0.0, 1.0, 1.0};
const Pixel Pixel::k_magenta{1.0, 0.0, 1.0};
const Pixel Pixel::k_yellow{1.0, 1.0, 0.0};
const Pixel Pixel::k_purple{0.4, 0.0, 0.8};
const Pixel Pixel::k_orange{1.0, 0.3, 0.0};
const Pixel Pixel::k_selected = Pixel::k_blue;

Pixel::Pixel(float r, float g, float b, float brightness) {
  setRed(r);
  setGreen(g);
  setBlue(b);
  setBrightness(brightness);
}

Pixel Pixel::hsvPixel(float h, float s, float v) {
  // https://stackoverflow.com/a/6930407
  float hh, p, q, t, ff;
  float r, g, b;
  long i;

  if(s <= 0.0) {
      return Pixel(v, v, v);
  }

  hh = h;
  if (hh >= 360.0) {
    hh = 0.0;
  }

  hh /= 60.0;
  i = (long)hh;
  ff = hh - i;
  p = v * (1.0 - s);
  q = v * (1.0 - (s * ff));
  t = v * (1.0 - (s * (1.0 - ff)));

  switch(i) {
  case 0:
      r = v;
      g = t;
      b = p;
      break;
  case 1:
      r = q;
      g = v;
      b = p;
      break;
  case 2:
      r = p;
      g = v;
      b = t;
      break;
  case 3:
      r = p;
      g = q;
      b = v;
      break;
  case 4:
      r = t;
      g = p;
      b = v;
      break;
  case 5:
  default:
      r = v;
      g = p;
      b = q;
      break;
  }
  
  return Pixel(r, g, b);  
}

// static float hueToRgb(p, q, t) {
//   if(t < 0.0) t += 1.0;
//   if(t > 1.0) t -= 1.0;
//   if(t < 1.0/6.0) return p + (q - p) * 6.0 * t;
//   if(t < 1.0/2.0) return q;
//   if(t < 2.0/3.0) return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
//   return p;
// }

// static Pixel* Pixel::hslPixel(float h, float s, float l) {
//   // https://stackoverflow.com/a/9493060
//   float r, g, b;

//   if (s == 0) {
//       r = g = b = l; // achromatic
//   } else {
//       var hue2rgb = function hue2rgb(p, q, t) {
//           if(t < 0) t += 1;
//           if(t > 1) t -= 1;
//           if(t < 1/6) return p + (q - p) * 6 * t;
//           if(t < 1/2) return q;
//           if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
//           return p;
//       }

//       var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
//       var p = 2 * l - q;
//       r = hue2rgb(p, q, h + 1/3);
//       g = hue2rgb(p, q, h);
//       b = hue2rgb(p, q, h - 1/3);
//   }

//   return new Pixel(Math.round(r * 255), Math.round(g * 255), Math.round(b * 255));
// }

float Pixel::getLuminance() {
  // https://stackoverflow.com/a/596243
  return (0.299 * pow(Pixel::m_r, 2.0) + 0.587 * pow(Pixel::m_g, 2.0) + 0.114 * pow(Pixel::m_b, 2.0));
}

void Pixel::setBrightness(float brightness) {
  if (brightness < 0.0) {
    m_brightness = 0.0;
  } else if (brightness > 1.0) {
    m_brightness = 1.0;
  } else {
    m_brightness = brightness;
  }
}

float Pixel::getRed() const {
  return m_r * m_brightness;
}

float Pixel::getGreen() const {
  return m_g * m_brightness;
}

float Pixel::getBlue() const {
  return m_b * m_brightness;
}

void Pixel::setRed(float r) {
  if (r < 0.0) {
    m_r = 0.0;
  } else if (r > 1.0) {
    m_r = 1.0;
  } else {
    m_r = r;
  }
}

void Pixel::setGreen(float g) {
  if (g < 0.0) {
    m_g = 0.0;
  } else if (g > 1.0) {
    m_g = 1.0;
  } else {
    m_g = g;
  }
}

void Pixel::setBlue(float b) {
  if (b < 0.0) {
    m_b = 0.0;
  } else if (b > 1.0) {
    m_b = 1.0;
  } else {
    m_b = b;
  }
}

bool Pixel::operator== (const Pixel &p) {
  return (std::abs(m_r - p.getRed()) < EPS);
}