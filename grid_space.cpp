#include "grid_space.h"
#include <cstring>

GridSpace::GridSpace() {
  m_grid.resize(GridPosition::kMaxX + 1, std::vector<GridSelection>(GridPosition::kMaxY + 1, GridSelection::NONE));
}

void GridSpace::resetGrid() {
  for (int x = GridPosition::kMinX; x <= GridPosition::kMaxX; x++) {
    for (int y = GridPosition::kMinY; y <= GridPosition::kMaxY; y++) {
      m_grid[x][y] = GridSelection::NONE;
    }
  }
}

void GridSpace::select(int x, int y) {
  if (!validateXY(x, y)) {
    return;
  }

  m_grid[x][y] = GridSelection(GridSelection::SELECTED);
}

void GridSpace::deselect(int x, int y) {
  m_grid[x][y] = GridSelection(GridSelection::NONE);
}

void GridSpace::deselectAll() {
  for (int x = GridPosition::kMinX; x <= GridPosition::kMaxX; x++) {
    for (int y = GridPosition::kMinY; y <= GridPosition::kMaxY; y++) {
      deselect(x, y);
    }
  }
}

GridSelection GridSpace::getNodeState(int x, int y) {
  if (!validateXY(x, y)) {
    return GridSelection::NONE;
  }

  return m_grid[x][y];
}


Pixel GridSpace::getNodeDefaultPixel(int x, int y) {
  return getNodeState(x, y).getDefaultPixel(GridPosition(x, y), false);
}

bool GridSpace::validateXY(int x, int y) {
  return x >= GridPosition::kMinX && x <= GridPosition::kMaxX &&
      y >= GridPosition::kMinY && y <= GridPosition::kMaxY;
}