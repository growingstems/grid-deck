#include <stdint.h>
#include "controls.h"
#include "grid_space.h"

#pragma once

class RobotPacket {
  public:
    RobotPacket(GridSpace gridSpace, Controls::IntakePositionsEnum intakePosition, Controls::IntakeAdjustEnum intakeAdjust, 
      Controls::BumpsEnum bump, bool diableSensors, bool stackMode, bool stackMode2, bool fixCone);
    uint8_t* getBytes();
  private:
    GridSpace m_gridSpace;
    Controls::IntakePositionsEnum m_intakePosition;
    Controls::IntakeAdjustEnum m_intakeAdjust;
    Controls::BumpsEnum m_bump;
    bool m_disableSensors;
    bool m_stackMode;
    bool m_stackMode2;
    bool m_fixCone;
};