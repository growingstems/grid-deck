#include "robot_packet.h"

typedef union {
  struct {
    uint8_t intakePos: 3;
    uint8_t unused1: 1;
    uint8_t fixCone: 1;
    uint8_t intakeAdjust: 2;
    uint8_t unused2: 1;
  } sections;
  uint8_t data;
} byte0_t;

typedef union {
  struct {
    uint8_t armBumps: 3;
    uint8_t unused1: 1;
    uint8_t stackMode: 1;
    uint8_t stackMode2: 1;
    uint8_t disableSensors: 1;
    uint8_t unused2: 1;
  } sections;
  uint8_t data;
} byte1_t;

typedef union {
  struct {
    uint8_t grid0_0: 1;
    uint8_t grid1_0: 1;
    uint8_t grid2_0: 1;
    uint8_t unused1: 1;
    uint8_t grid3_0: 1;
    uint8_t grid4_0: 1;
    uint8_t grid5_0: 1;
    uint8_t unused2: 1;
  } sections;
  uint8_t data;
} byte2_t;

typedef union {
  struct {
    uint8_t grid6_0: 1;
    uint8_t grid7_0: 1;
    uint8_t grid8_0: 1;
    uint8_t unused1: 1;
    uint8_t grid0_1: 1;
    uint8_t grid1_1: 1;
    uint8_t grid2_1: 1;
    uint8_t unused2: 1;
  } sections;
  uint8_t data;
} byte3_t;

typedef union {
  struct {
    uint8_t grid3_1: 1;
    uint8_t grid4_1: 1;
    uint8_t grid5_1: 1;
    uint8_t unused1: 1;
    uint8_t grid6_1: 1;
    uint8_t grid7_1: 1;
    uint8_t grid8_1: 1;
    uint8_t unused2: 1;
  } sections;
  uint8_t data;
} byte4_t;

typedef union {
  struct {
    uint8_t grid0_2: 1;
    uint8_t grid1_2: 1;
    uint8_t grid2_2: 1;
    uint8_t unused1: 1;
    uint8_t grid3_2: 1;
    uint8_t grid4_2: 1;
    uint8_t grid5_2: 1;
    uint8_t unused2: 1;
  } sections;
  uint8_t data;
} byte5_t;

typedef union {
  struct {
    uint8_t grid6_2: 1;
    uint8_t grid7_2: 1;
    uint8_t grid8_2: 1;
    uint8_t unused: 5;
  } sections;
  uint8_t data;
} byte6_t;

RobotPacket::RobotPacket(GridSpace gridSpace, Controls::IntakePositionsEnum intakePosition, Controls::IntakeAdjustEnum intakeAdjust, Controls::BumpsEnum bump, bool disableSensors, bool stackMode, bool stackMode2, bool fixCone) {
  m_gridSpace = gridSpace;
  m_intakePosition = intakePosition;
  m_intakeAdjust = intakeAdjust;
  m_bump = bump;
  m_disableSensors = disableSensors;
  m_stackMode = stackMode;
  m_stackMode2 = stackMode2;
  m_fixCone = fixCone;
}


uint8_t* RobotPacket::getBytes() {
  byte0_t byte0 = {0};
  byte0.sections.intakePos = m_intakePosition;
  byte0.sections.intakeAdjust = m_intakeAdjust;
  byte0.sections.fixCone = m_fixCone;

  byte0.sections.unused1 = 0;
  byte0.sections.unused2 = 0;

  byte1_t byte1 = {0};
  byte1.sections.armBumps = m_bump;
  byte1.sections.disableSensors = m_disableSensors;
  byte1.sections.stackMode = m_stackMode;
  byte1.sections.stackMode2 = m_stackMode2;

  byte1.sections.unused1 = 0;
  byte1.sections.unused2 = 0;

  byte2_t byte2 = {0};
  byte2.sections.grid0_0 = m_gridSpace.getNodeState(0, 0).getEnum() == GridSelection::SELECTED;
  byte2.sections.grid1_0 = m_gridSpace.getNodeState(1, 0).getEnum() == GridSelection::SELECTED;
  byte2.sections.grid2_0 = m_gridSpace.getNodeState(2, 0).getEnum() == GridSelection::SELECTED;
  byte2.sections.grid3_0 = m_gridSpace.getNodeState(3, 0).getEnum() == GridSelection::SELECTED;
  byte2.sections.grid4_0 = m_gridSpace.getNodeState(4, 0).getEnum() == GridSelection::SELECTED;
  byte2.sections.grid5_0 = m_gridSpace.getNodeState(5, 0).getEnum() == GridSelection::SELECTED;

  byte3_t byte3 = {0};
  byte3.sections.grid6_0 = m_gridSpace.getNodeState(6, 0).getEnum() == GridSelection::SELECTED;
  byte3.sections.grid7_0 = m_gridSpace.getNodeState(7, 0).getEnum() == GridSelection::SELECTED;
  byte3.sections.grid8_0 = m_gridSpace.getNodeState(8, 0).getEnum() == GridSelection::SELECTED;
  byte3.sections.grid0_1 = m_gridSpace.getNodeState(0, 1).getEnum() == GridSelection::SELECTED;
  byte3.sections.grid1_1 = m_gridSpace.getNodeState(1, 1).getEnum() == GridSelection::SELECTED;
  byte3.sections.grid2_1 = m_gridSpace.getNodeState(2, 1).getEnum() == GridSelection::SELECTED;

  byte4_t byte4 = {0};
  byte4.sections.grid3_1 = m_gridSpace.getNodeState(3, 1).getEnum() == GridSelection::SELECTED;
  byte4.sections.grid4_1 = m_gridSpace.getNodeState(4, 1).getEnum() == GridSelection::SELECTED;
  byte4.sections.grid5_1 = m_gridSpace.getNodeState(5, 1).getEnum() == GridSelection::SELECTED;
  byte4.sections.grid6_1 = m_gridSpace.getNodeState(6, 1).getEnum() == GridSelection::SELECTED;
  byte4.sections.grid7_1 = m_gridSpace.getNodeState(7, 1).getEnum() == GridSelection::SELECTED;
  byte4.sections.grid8_1 = m_gridSpace.getNodeState(8, 1).getEnum() == GridSelection::SELECTED;

  byte5_t byte5 = {0};
  byte5.sections.grid0_2 = m_gridSpace.getNodeState(0, 2).getEnum() == GridSelection::SELECTED;
  byte5.sections.grid1_2 = m_gridSpace.getNodeState(1, 2).getEnum() == GridSelection::SELECTED;
  byte5.sections.grid2_2 = m_gridSpace.getNodeState(2, 2).getEnum() == GridSelection::SELECTED;
  byte5.sections.grid3_2 = m_gridSpace.getNodeState(3, 2).getEnum() == GridSelection::SELECTED;
  byte5.sections.grid4_2 = m_gridSpace.getNodeState(4, 2).getEnum() == GridSelection::SELECTED;
  byte5.sections.grid5_2 = m_gridSpace.getNodeState(5, 2).getEnum() == GridSelection::SELECTED;


  byte6_t byte6 = {0};
  byte6.sections.grid6_2 = m_gridSpace.getNodeState(6, 2).getEnum() == GridSelection::SELECTED;
  byte6.sections.grid7_2 = m_gridSpace.getNodeState(7, 2).getEnum() == GridSelection::SELECTED;
  byte6.sections.grid8_2 = m_gridSpace.getNodeState(8, 2).getEnum() == GridSelection::SELECTED;

  byte6.sections.unused = 0;

  uint8_t* arr = new uint8_t[7];
  arr[0] = byte0.data;
  arr[1] = byte1.data;
  arr[2] = byte2.data;
  arr[3] = byte3.data;
  arr[4] = byte4.data;
  arr[5] = byte5.data;
  arr[6] = byte6.data;

  return arr;
}
