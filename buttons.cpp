#include "buttons.h"

#include "Adafruit_NeoTrellis.h"
#include "robot_packet.h"
#include "timer.h"
#include <cmath>

static const byte ROWS = 4;
static const byte COLS = 12;

static int gridSelectX = -1;
static int gridSelectY = -1;
static bool gridSelectChanged = false;

static int armBumpX = -1;
static int armBumpY = -1;
static bool armBumpChanged = false;

static int intakePositionX = -1;
static int intakePositionY = -1;
static bool intakePositionChanged = false;

static int intakeAdjustX = -1;
static int intakeAdjustY = -1;
static bool intakeAdjustChanged = false;

static int stackModeX = -1;
static int stackModeY = -1;
static bool stackModeChanged = false;

static int disableSensorsX = -1;
static int disableSensorsY = -1;
static bool disableSensorsChanged = false;

static int stopFlashX = -1;
static int stopFlashY = -1;
static bool stopFlashChanged = false;

static int fixConeX = -1;
static int fixConeY = -1;
static bool fixConeChanged = false;

union color {
  struct {
    uint8_t blue:8;
    uint8_t green:8;
    uint8_t red:8;
  } bit;
  uint32_t reg;
};

int getXFromNum(int num) {
  return num % COLS;
}

int getYFromNum(int num) {
  return num / COLS;
}

TrellisCallback gridSelectCb(keyEvent evt) {
  // Latch Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    gridSelectX = getXFromNum(evt.bit.NUM);
    gridSelectY = getYFromNum(evt.bit.NUM);
    gridSelectChanged = true;
  }

  return 0;
}

TrellisCallback armBumpCb(keyEvent evt) {
  // While Held Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    armBumpX = getXFromNum(evt.bit.NUM);
    armBumpY = getYFromNum(evt.bit.NUM);
    armBumpChanged = true;
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    armBumpX = -1;
    armBumpY = -1;
    armBumpChanged = true;
  }

  return 0;
}

TrellisCallback intakePositionCb(keyEvent evt) {
  // Latch Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    intakePositionX = getXFromNum(evt.bit.NUM);
    intakePositionY = getYFromNum(evt.bit.NUM);
    intakePositionChanged = true;
  }

  return 0;
}

TrellisCallback intakeAdjustCb(keyEvent evt) {
  // While Held Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    intakeAdjustX = getXFromNum(evt.bit.NUM);
    intakeAdjustY = getYFromNum(evt.bit.NUM);
    intakeAdjustChanged = true;
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    intakeAdjustX = -1;
    intakeAdjustY = -1;
    intakeAdjustChanged = true;
  }

  return 0;
}

TrellisCallback stackModeCb(keyEvent evt) {
  int x = getXFromNum(evt.bit.NUM);
  int y = getYFromNum(evt.bit.NUM);

  // Toggle Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    // Normal Button
    if (x == 4 && y == 0) {
      if (stackModeX < 0 || stackModeY < 0) {
        // Enable Stack Mode 1
        stackModeX = 4;
        stackModeY = 0;
      } else {
        // Clicking Stack Mode 1 button always clears Stack Mode
        stackModeX = -1;
        stackModeY = -1;
      }      
    } else if (x == 5 && y == 0) {
      if (stackModeX == 4 || stackModeY == 0) {
        // We were on Stack Mode 1, so upgrade to 2
        stackModeX = 5;
        stackModeY = 0;
      }
    }
    
    stackModeChanged = true;
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    if (x == 5 && y == 0) {
      // Go back to Stack Mode 1 if in Stack Mode 2
      stackModeX = 4;
      stackModeY = 0;
      
      stackModeChanged = true;
    }
  }

  return 0;
}

TrellisCallback disableSensorsCb(keyEvent evt) {
  // Latch Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    if (disableSensorsX < 0 || disableSensorsY < 0) {
      disableSensorsX = getXFromNum(evt.bit.NUM);
      disableSensorsY = getYFromNum(evt.bit.NUM);
    } else {
      disableSensorsX = -1;
      disableSensorsY = -1;
    }
    disableSensorsChanged = true;
  }

  return 0;
}

TrellisCallback stopFlashCb(keyEvent evt) {
  // While Held Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    stopFlashX = getXFromNum(evt.bit.NUM);
    stopFlashY = getYFromNum(evt.bit.NUM);
    stopFlashChanged = true;
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    stopFlashX = -1;
    stopFlashY = -1;
    stopFlashChanged = true;
  }

  return 0;
}

TrellisCallback fixConeCb(keyEvent evt) {
  // While Held Action
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    fixConeX = getXFromNum(evt.bit.NUM);
    fixConeY = getYFromNum(evt.bit.NUM);
    fixConeChanged = true;
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    fixConeX = -1;
    fixConeY = -1;
    fixConeChanged = true;
  }

  return 0;
}

//create a matrix of trellis panels
Adafruit_NeoTrellis t_array[ROWS/4][COLS/4] = {
  { Adafruit_NeoTrellis(0x2E + 0x01), Adafruit_NeoTrellis(0x2E + 0x02), Adafruit_NeoTrellis(0x2E + 0x04) }
};

Adafruit_MultiTrellis trellis = Adafruit_MultiTrellis((Adafruit_NeoTrellis *)t_array, ROWS/4, COLS/4);

void Buttons::begin() {
  if(!trellis.begin()){
    Serial.println("failed to begin trellis");
    while(1) delay(1);
  }

  int x;
  int y;

  // Grid
  for (x = GridPosition::kMinX; x <= GridPosition::kMaxX; x++) {
    for (y = GridPosition::kMinY; y <= GridPosition::kMaxY; y++) {
      int buttonX = x + k_gridOffsetX;
      int buttonY = y + k_gridOffsetY;

      trellis.activateKey(buttonX, buttonY, SEESAW_KEYPAD_EDGE_RISING, true);
      trellis.registerCallback(buttonX, buttonY, gridSelectCb);
    }
  }

  // Intake Adjust
  y = 0;
  for(x = 10; x <= 11; x++) {
    trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
    trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
    trellis.registerCallback(x, y, intakeAdjustCb);
  }

  // Arm Bumps
  y = 0;
  for (x = 2; x <= 3; x++) {
    trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
    trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
    trellis.registerCallback(x, y, armBumpCb);
  }

  // Intake Position
  x = 0;
  y = 0;
  trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
  trellis.registerCallback(x, y, intakePositionCb);
  for (x = 0; x <= 1; x++) {
    for (y = 1; y <= 3; y++) {
      trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
      trellis.registerCallback(x, y, intakePositionCb);
    }
  }

  // Stack Mode
  y = 0;
  for (x = 4; x <= 5; x++) {
    trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
    trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
    trellis.registerCallback(x, y, stackModeCb);
  }

  // Disable Sensors
  x = 6;
  y = 0;
  trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
  trellis.registerCallback(x, y, disableSensorsCb);

  // Stop Flash
  y = 3;
  x = 11;
  trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
  trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
  trellis.registerCallback(x, y, stopFlashCb);

  // Fix Cone
  y = 1;
  x = 11;
  trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
  trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
  trellis.registerCallback(x, y, fixConeCb);
}

void Buttons::setAllPixels(uint32_t color) {
  for(int y=0; y<ROWS; y++){
    for(int x=0; x<COLS; x++){
      trellis.setPixelColor(x, y, color); //addressed with x,y
    }
  }

  trellis.show(); //show all LEDs
}

void Buttons::clear() {
  for(int y=0; y<ROWS; y++) {
    for(int x=0; x<COLS; x++){
      setPixel(Pixel::k_off, x, y);
    }
  }
}

void Buttons::setPixel(Pixel pixel, int x, int y) {
  color color;
  color.bit.red = (uint8_t)(pixel.getRed() * 255.0);
  color.bit.green = (uint8_t)(pixel.getGreen() * 255.0);
  color.bit.blue = (uint8_t)(pixel.getBlue() * 255.0);

  trellis.setPixelColor(x, y, color.reg);
}

void Buttons::setPixel(GridSelection selection, GridPosition position, int x, int y, bool blinkBlank) {
  setPixel(selection.getDefaultPixel(position, blinkBlank), x, y);
}

void Buttons::updatePixels() {
  int x;
  int y;
  
  clear();

  // Grid
  for (x = GridPosition::kMinX; x <= GridPosition::kMaxX; x++) {
    for (y = GridPosition::kMinY; y <= GridPosition::kMaxY; y++) {
      setPixel(m_grid.getNodeDefaultPixel(x, y), x + k_gridOffsetX, y + k_gridOffsetY);
    }
  }

  // Intake Adjust
  setPixel(Pixel::k_purple, 10, 0);
  setPixel(Pixel::k_yellow, 11, 0);

  y = 0;
  for(x = 10; x <= 11; x++) {
    if (x == intakeAdjustX && y == intakeAdjustY) {
      setPixel(Pixel::k_selected, x, y);
    }
  }

  // Arm Bumps
  y = 0;
  for (x = 2; x <= 3; x++) {
    if (x == armBumpX && y == armBumpY) {
      setPixel(Pixel::k_selected, x, y);
    } else {
      setPixel(Pixel::k_red, x, y);
    }
  }

  // Intake Position
  if (m_flash.getPulseState()) {
    setPixel(Pixel::k_purple, 1, 3);
    setPixel(Pixel::k_yellow, 1, 2);
    setPixel(Pixel::k_red, 1, 1);
  } else  {
    setPixel(Pixel::k_off, 1, 3);
    setPixel(Pixel::k_off, 1, 2);
    setPixel(Pixel::k_off, 1, 1);

    x = 1;
    for (y = 1; y <= 3; y++) {
      if (x == intakePositionX && y == intakePositionY) {
        setPixel(Pixel::k_selected, x, y);
      }
    }
  }

  setPixel(Pixel::k_purple, 0, 3);
  setPixel(Pixel::k_yellow, 0, 2);
  setPixel(Pixel::k_green, 0, 1);
  setPixel(Pixel::k_red, 0, 0);

  x = 0;
  for (y = 0; y <= 3; y++) {
    if (x == intakePositionX && y == intakePositionY) {
      setPixel(Pixel::k_selected, x, y);
    }
  }

  // Stack Mode
  if (4 == stackModeX && 0 == stackModeY) {
    setPixel(Pixel::k_selected, 4, 0);
    setPixel(m_fastRainbow.update(), 5, 0);
  } else if (5 == stackModeX && 0 == stackModeY) {
    setPixel(Pixel::k_selected, 4, 0);
    setPixel(Pixel::k_selected, 5, 0);
  } else {
    setPixel(m_rainbow.update(), 4, 0);
    setPixel(Pixel::k_off, 5, 0);
  }

  // Disable Sensors
  x = 6;
  y = 0;
  if (x == disableSensorsX && y == disableSensorsY) {
    setPixel(Pixel::k_selected, x, y);
  } else {
    setPixel(Pixel::k_red, x, y);
  }

  // Fix Cone
  x = 11;
  y = 1;
  if (x == fixConeX && y == fixConeY) {
    setPixel(Pixel::k_selected, x, y);
  } else {
    setPixel(Pixel::k_orange, x, y);
  }

  // Heartbeat Watchdog
  if (!m_watchdogFed && stopFlashX < 0 && stopFlashY < 0) {
    for(int x = 0; x < 12; x++) {
      for(int y = 0; y < 4; y++) {
        if (m_watchdogNotFedFlash.getPulseState()) {
          Pixel p = Pixel::k_red;
          p.setBrightness(0.01);
          setPixel(p, x, y);
        }
      }
    }
  }

  trellis.show();
}

void Buttons::update() {
  trellis.read();
  
  if (gridSelectChanged) {
    int gridX = gridSelectX - k_gridOffsetX;
    int gridY = gridSelectY - k_gridOffsetY;

    m_grid.deselectAll();
    m_grid.select(gridX, gridY);

    gridSelectChanged = false;
  }

  if (armBumpChanged) {
    if (armBumpY == 0) {
      if (armBumpX == 2) {
        m_armBump = Controls::LEFT;
      } else if (armBumpX == 3) {
        m_armBump = Controls::RIGHT;
      } else {
        m_armBump = Controls::BUMPS_NONE;
      }
    } else {
      m_armBump = Controls::BUMPS_NONE;
    }
    armBumpChanged = false;
  }

  if (intakeAdjustChanged) {
    if (intakeAdjustX == 10) {
      m_intakeAdjust = Controls::INTAKE_CUBE_OUT;
    } else if (intakeAdjustX == 11) {
      m_intakeAdjust = Controls::INTAKE_CONE_OUT;
    } else {
      m_intakeAdjust = Controls::INTAKE_ADJUST_NONE;
    }

    intakeAdjustChanged = false;
  }

  if (intakePositionChanged) {
    if (intakePositionX == 0) {
      if (intakePositionY == 0) {
        m_intakePosition = Controls::LIAM_TIPPED_YELLOW;
      } else if (intakePositionY == 1) {
        m_intakePosition = Controls::TIPPED_YELLOW;
      } else if (intakePositionY == 2) {
        m_intakePosition = Controls::GROUND_YELLOW;
      } else if (intakePositionY == 3) {
        m_intakePosition = Controls::GROUND_PURPLE;
      } else {
        m_intakePosition = Controls::INTAKE_POS_NONE;
      }
    } else if (intakePositionX == 1) {
      if (intakePositionY == 1) {
        m_intakePosition = Controls::SHELF_YELLOW;
      } else if (intakePositionY == 2) {
        m_intakePosition = Controls::CHUTE_YELLOW;
      } else if (intakePositionY == 3) {
        m_intakePosition = Controls::CHUTE_PURPLE;
      } else {
        m_intakePosition = Controls::INTAKE_POS_NONE;
      }
    } else {
      m_intakePosition = Controls::INTAKE_POS_NONE;
    }

    intakePositionChanged = false;
  }

  if (stackModeChanged) {
    if (stackModeX == 4 && stackModeY == 0) {
      m_stackMode = true;
      m_stackMode2 = false;
    } else if (stackModeX == 5 && stackModeY == 0) {
      m_stackMode = true;
      m_stackMode2 = true;
    } else {
      m_stackMode = false;
      m_stackMode2 = false;
    }

    stackModeChanged = false;
  }

  if (disableSensorsChanged) {
    m_disableSensors = (disableSensorsX >= 0 && disableSensorsY >= 0);
    disableSensorsChanged = false;
  }

  if (fixConeChanged) {
    m_fixCone = (fixConeX >= 0 && fixConeY >= 0);
    fixConeChanged = false;
  }

  updatePixels();
}

uint8_t* Buttons::getBytes() {
  RobotPacket packet = RobotPacket(m_grid, m_intakePosition, m_intakeAdjust, m_armBump, m_disableSensors, m_stackMode, m_stackMode2, m_fixCone);
  return packet.getBytes();
}

void Buttons::feedHeartbeatWatchdog(bool feed) {
  float currTime = Timer::getCurrentTime();
  float timeSinceLastFed = currTime - m_watchdogLastFed;
  
  if (feed) {
    m_watchdogLastFed = currTime;
  }

  m_watchdogFed = (timeSinceLastFed < k_watchdogExceededTime);  
}