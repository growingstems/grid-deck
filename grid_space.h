#include <vector>

#include "grid_selection.h"
#include "grid_position.h"

#pragma once

class GridSpace {
  public:
    GridSpace();

    void select(int x, int y);
    void deselect(int x, int y);
    void deselectAll();

    GridSelection getNodeState(int x, int y);
    Pixel getNodeDefaultPixel(int x, int y);

  private:
    std::vector<std::vector<GridSelection>> m_grid;

    bool validateXY(int x, int y);
    void resetGrid();
};