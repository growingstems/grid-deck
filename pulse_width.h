#include "timer.h"

#pragma once

class PulseWidth {
  public:
    PulseWidth(float period, float dutyCycle);
    bool getPulseState();
  private:
    float m_period;
    float m_dutyCycle;
    Timer m_timer = Timer();
};