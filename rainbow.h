#include "Pixel.h"

#pragma once

class Rainbow {
  public:
    Rainbow(float changePerSecond);
    Pixel update();
  private:
    float m_lastTime_s;
    float m_changePerSecond;
    float m_currentHue;
};