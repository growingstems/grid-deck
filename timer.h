#pragma once

class Timer {
  public:
    Timer();

    void start();
    void stop();
    void reset();
    
    float get();
    bool hasElapsed(float time);
    bool hasPeriodPassed(float time);
    
    static float getCurrentTime();

  private:
    float m_startTime;
    bool m_running = false;
};