#include "pixel.h"

#pragma once

class GridPosition {
  public:
    enum NodePositionEnum {
      LEFT_UPPER_LEFT,
      LEFT_UPPER_CENTER,
      LEFT_UPPER_RIGHT,
      LEFT_CENTER_LEFT,
      LEFT_CENTER,
      LEFT_CENTER_RIGHT,
      LEFT_BOTTOM_LEFT,
      LEFT_BOTTOM_CENTER,
      LEFT_BOTTOM_RIGHT,
      COOP_UPPER_LEFT,
      COOP_UPPER_CENTER,
      COOP_UPPER_RIGHT,
      COOP_CENTER_LEFT,
      COOP_CENTER,
      COOP_CENTER_RIGHT,
      COOP_BOTTOM_LEFT,
      COOP_BOTTOM_CENTER,
      COOP_BOTTOM_RIGHT,
      RIGHT_UPPER_LEFT,
      RIGHT_UPPER_CENTER,
      RIGHT_UPPER_RIGHT,
      RIGHT_CENTER_LEFT,
      RIGHT_CENTER,
      RIGHT_CENTER_RIGHT,
      RIGHT_BOTTOM_LEFT,
      RIGHT_BOTTOM_CENTER,
      RIGHT_BOTTOM_RIGHT
    };

    enum NodePositionTypeEnum {
      CONE,
      CUBE,
      HYBRID
    };

    static const int kMinX = 0;
    static const int kMaxX = 8;
    static const int kMinY = 0;
    static const int kMaxY = 2;

    GridPosition(const NodePositionEnum &pos);
    GridPosition(const int &x, const int &y);

    int getX() const;
    int getY() const;
    NodePositionEnum getEnum() const;
    bool isValid() const;

    NodePositionTypeEnum getPositionType();
    Pixel getDefaultPixel();

  private:
    int mX;
    int mY;
    NodePositionEnum mPosition;
    bool mValid;
};